﻿using UnityEngine;
using System.Collections;

public class DrawHands : MonoBehaviour {
	public Texture2D texCursor;
	public int size;
	public Vector2 positionHand1;
	public Vector2 positionHandUpper1;
	public Vector2 positionHand2;
	public Vector2 positionHandUpper2;
	public float smoothing = 0.7f;
	
	Vector2 frameSize;
	
	private PXCUPipeline.Mode mode = PXCUPipeline.Mode.GESTURE;
	private PXCUPipeline pp;
	
	void Start() {
		pp = new PXCUPipeline();
		pp.Init(mode);
		int[] size = new int[2];
		pp.QueryLabelMapSize(size);
		Vector2 s = new Vector2(size[0], size[1]);
		frameSize = s;
	}
	
	void OnDisable() {
		pp.Dispose();
	}

	void Update() {
		if (!pp.AcquireFrame(false)) return;
		PXCMGesture.GeoNode data;
		if (pp.QueryGeoNode(PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_PRIMARY, out data)) {
			// Get cursor position in terms of GUI coordinate system (bottom-left origin)
			Vector2 h = new Vector2(Screen.width - data.positionImage.x * Screen.width / frameSize.x,
				Screen.height - data.positionImage.y * Screen.height / frameSize.y);
			Vector2 p = (1.0f - smoothing)*h + smoothing*positionHand1;
			positionHand1 = p;
			Debug.Log(data.openness);
		}
		if (pp.QueryGeoNode(PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_PRIMARY |
			PXCMGesture.GeoNode.Label.LABEL_HAND_UPPER, out data)) {
			// Get cursor position in terms of GUI coordinate system (bottom-left origin)
			Vector2 h = new Vector2(Screen.width - data.positionImage.x * Screen.width / frameSize.x,
				Screen.height - data.positionImage.y * Screen.height / frameSize.y);
			Vector2 p = (1.0f - smoothing)*h + smoothing*positionHandUpper1;
			positionHandUpper1 = p;
		}
		
		if (pp.QueryGeoNode(PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_SECONDARY, out data)) {
			// Get cursor position in terms of GUI coordinate system (bottom-left origin)
			Vector2 h = new Vector2(Screen.width - data.positionImage.x * Screen.width / frameSize.x,
				Screen.height - data.positionImage.y * Screen.height / frameSize.y);
			Vector2 p = (1.0f - smoothing)*h + smoothing*positionHand2;
			positionHand2 = p;
			Debug.Log(data.openness);
		}
		if (pp.QueryGeoNode(PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_SECONDARY |
			PXCMGesture.GeoNode.Label.LABEL_HAND_UPPER, out data)) {
			// Get cursor position in terms of GUI coordinate system (bottom-left origin)
			Vector2 h = new Vector2(Screen.width - data.positionImage.x * Screen.width / frameSize.x,
				Screen.height - data.positionImage.y * Screen.height / frameSize.y);
			Vector2 p = (1.0f - smoothing)*h + smoothing*positionHandUpper2;
			positionHandUpper2 = p;
		}
		
		pp.ReleaseFrame();
	}
	
	void OnGUI() {
		// Draw the cursor in Rect coordinate system (top-left origin)
		Rect r = new Rect(positionHand1.x - size/2, Screen.height - positionHand1.y - size/2, size, size);
		GUI.DrawTexture(r, texCursor);
		r = new Rect(positionHandUpper1.x - size/2, Screen.height - positionHandUpper1.y - size/2, size, size);
		GUI.DrawTexture(r, texCursor);
		r = new Rect(positionHand2.x - size/2, Screen.height - positionHand2.y - size/2, size, size);
		GUI.DrawTexture(r, texCursor);
		r = new Rect(positionHandUpper2.x - size/2, Screen.height - positionHandUpper2.y - size/2, size, size);
		GUI.DrawTexture(r, texCursor);
	}
}
