﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PerceptualCursor {
	public Vector2 position;
	public float zRotation;
	public Vector3 positionWorld;
	public Vector2 upperPosition;
	public int layerNumber = 0;
		
	public int size = 40;

	Texture texDefault;
	Texture texHover;
	Texture texGrab;
	Texture texCurrent;
	
	GameObject objPrev;
	GameObject obj;
	
	bool grabbed = false;
	bool visible = false;
	
	public PerceptualCursor() {
		texCurrent = texDefault;
		onHover += doNothing;
		onLeave += doNothing;
		onGrab += doNothing;
		onRelease += doNothing;
	}
	
	public void updateRotation() {
		// Calculate rotation from position and upperPosition
		Vector2 rotvec = upperPosition - position;
		float rot = Vector2.Angle(Vector2.up, rotvec);
		
		if (Vector2.Dot(rotvec, Vector2.right) > 0) {
			rot = -rot;
		}
		zRotation = zRotation*0.5f + rot*0.5f;
	}
	
	public void hide() {
		visible = false;
	}
	
	public void show() {
		visible = true;
	}

	public bool isGrabbed() {
		return grabbed;
	}
	
	public GameObject getGrabbedObject() {
		return obj;
	}
	
	public void setTextures(Texture d, Texture h, Texture g) {
		texDefault = d;
		texHover = h;
		texGrab = g;
		texCurrent = texDefault;
	}
	
	public void draw() {
		if (!visible) {
			return;
		}
		
		Rect r;
		/*
		// If we are grabbing an object draw the cursor at the object's center 
		if (grabbed && obj) {
			r = new Rect(position.x + positionOffset.x - size/2,
				Screen.height - (position.y + positionOffset.y) - size/2, size, size);
		} else {
			r = new Rect(position.x - size/2, Screen.height - position.y - size/2, size, size);
		}
		*/
		r = new Rect(position.x - size/2, Screen.height - position.y - size/2, size, size);
		GUI.DrawTexture(r, texCurrent);
	}
	
	public void update() {
		if (!grabbed) {
			Ray ray = Camera.main.ScreenPointToRay(position);
			RaycastHit hit = new RaycastHit();
			if (Physics.Raycast(ray, out hit, 1000.0f, (1<<layerNumber))) {
				obj = hit.collider.gameObject;
				if (obj != objPrev) {
					if (objPrev) {
						onLeave(objPrev);
					}
					onHover(obj);
					objPrev = obj;
				}
				texCurrent = texHover;
			} else {
				obj = null;
				if (objPrev) {
					onLeave(objPrev);
					objPrev = null;
				}
				texCurrent = texDefault;
			}
		}
	}
	
	public void grab() {
		if (!grabbed) {
			grabbed = true;
			if (obj) {
				onGrab(obj);
				texCurrent = texGrab;
			} else {
				texCurrent = texDefault;
			}
		}
	}
	
	public void release() {
		if (grabbed) {
			grabbed = false;
			if (obj) {
				onRelease(obj);
			}
			texCurrent = texDefault;
		}
	}
	
#region Events
	public delegate void onHoverEvent(GameObject obj);
	public event onHoverEvent onHover;
	public delegate void onLeaveEvent(GameObject obj);
	public event onLeaveEvent onLeave;
	public delegate void onGrabEvent(GameObject obj);
	public event onGrabEvent onGrab;
	public delegate void onReleaseEvent(GameObject obj);
	public event onReleaseEvent onRelease;
	
	void doNothing(GameObject obj) {}
#endregion
}

public class Perceptual : MonoBehaviour {
	// Singleton
	private static Perceptual instance;
	private Perceptual() {}
	
	// Instance
	public static Perceptual Instance {
		get {
			if (instance == null) {
				instance = GameObject.FindObjectOfType(typeof(Perceptual)) as Perceptual;
			}
			return instance;
		}
	}
	
	public int layerNumber;
	public Texture curDefault;
	public Texture curHover;
	public Texture curGrab;
	public float smoothing = 0.7f;
	public int grabOpennessThreshold = 15;
	
	private PXCUPipeline.Mode mode = PXCUPipeline.Mode.GESTURE;
	private PXCUPipeline pp;
	
	Vector2 frameSize;
	
	PerceptualCursor cursor1 = new PerceptualCursor();
	PerceptualCursor cursor2 = new PerceptualCursor();
	
#region Events
	public delegate void onHoverEvent(int idx, GameObject obj);
	public event onHoverEvent onHover;
	public delegate void onLeaveEvent(int idx, GameObject obj);
	public event onLeaveEvent onLeave;
	public delegate void onGrabEvent(int idx, GameObject obj);
	public event onGrabEvent onGrab;
	public delegate void onReleaseEvent(int idx, GameObject obj);
	public event onReleaseEvent onRelease;
	
	void onHover1(GameObject obj) {
		if (onHover != null) onHover(1, obj);
	}
	void onHover2(GameObject obj) {
		if (onHover != null) onHover(2, obj);
	}
	void onLeave1(GameObject obj) {
		if (onLeave != null) onLeave(1, obj);
	}
	void onLeave2(GameObject obj) {
		if (onLeave != null) onLeave(2, obj);
	}
	void onGrab1(GameObject obj) {
		if (onGrab != null) onGrab(1, obj);
	}
	void onGrab2(GameObject obj) {
		if (onGrab != null) onGrab(2, obj);
	}
	void onRelease1(GameObject obj) {
		if (onRelease != null) onRelease(1, obj);
	}
	void onRelease2(GameObject obj) {
		if (onRelease != null) onRelease(2, obj);
	}
#endregion
	
	public Vector2 getCursorPosition(int idx) {
		if (idx == 1) {
			return cursor1.position;
		} else if (idx == 2) {
			return cursor2.position;
		} else {
			return new Vector2();
		}
	}
	
	public Vector3 getCursorPositionWorld(int idx) {
		if (idx == 1) {
			return cursor1.positionWorld;
		} else if (idx == 2) {
			return cursor2.positionWorld;
		} else {
			return new Vector3();
		}
	}
	
	public float getCursorZRotation(int idx) {
		if (idx == 1) {
			return cursor1.zRotation;
		} else if (idx == 2) {
			return cursor2.zRotation;
		} else {
			return 0;
		}
	}
	
	void Start() {
		pp = new PXCUPipeline();
		pp.Init(mode);
		int[] size = new int[2];
		pp.QueryLabelMapSize(size);
		Vector2 s = new Vector2(size[0], size[1]);
		frameSize = s;
		
		// Setup events
		cursor1.onHover += onHover1;
		cursor2.onHover += onHover2;
		cursor1.onLeave += onLeave1;
		cursor2.onLeave += onLeave2;
		cursor1.onGrab += onGrab1;
		cursor2.onGrab += onGrab2;
		cursor1.onRelease += onRelease1;
		cursor2.onRelease += onRelease2;
		
		// Set textures
		cursor1.setTextures(curDefault, curHover, curGrab);
		cursor2.setTextures(curDefault, curHover, curGrab);
		
		// Set raycast layer
		cursor1.layerNumber = layerNumber;
		cursor2.layerNumber = layerNumber;
	}
	
	void OnDisable() {
		pp.Dispose();
	}
	
	void Update() {
		Screen.showCursor = false;
		
		detectHands();
		cursor1.update();
		cursor2.update();
	}
	
	void OnGUI() {
		cursor1.draw();
		cursor2.draw();
	}
	
	void detectHands() {
		if (pp.AcquireFrame(false)) {
			PXCMGesture.GeoNode data;
			
			if (pp.QueryGeoNode(PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_PRIMARY, out data)) {
				// Get cursor position in terms of GUI coordinate system (bottom-left origin)
				Vector2 h = new Vector2(Screen.width - data.positionImage.x * Screen.width / frameSize.x,
					Screen.height - data.positionImage.y * Screen.height / frameSize.y);
				Vector2 p = (1.0f-smoothing)*h + smoothing*cursor1.position;
				cursor1.position = p;
				
				// Get cursor world position
				cursor1.positionWorld =
					new Vector3(data.positionWorld.x, data.positionWorld.y, data.positionWorld.z);
				
				// Grab
				if (data.openness < grabOpennessThreshold) {
					cursor1.grab();
				} else {
					cursor1.release();
				}
				
				// Update upper hand position
				if (pp.QueryGeoNode(PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_PRIMARY |
					PXCMGesture.GeoNode.Label.LABEL_HAND_UPPER, out data)) {
					// Get cursor position in terms of GUI coordinate system (bottom-left origin)
					Vector2 uh = new Vector2(Screen.width - data.positionImage.x * Screen.width / frameSize.x,
						Screen.height - data.positionImage.y * Screen.height / frameSize.y);
					Vector2 up = (1.0f-smoothing)*uh + smoothing*cursor1.upperPosition;
					cursor1.upperPosition = up;
					cursor1.updateRotation();
				}

				cursor1.show();
			} else {
				cursor1.hide();
			}
	
			if (pp.QueryGeoNode(PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_SECONDARY, out data)) {
				// Get cursor position in terms of GUI coordinate system (bottom-left origin)
				Vector2 h = new Vector2(Screen.width - data.positionImage.x * Screen.width / frameSize.x,
					Screen.height - data.positionImage.y * Screen.height / frameSize.y);
				Vector2 p = (1.0f-smoothing)*h + smoothing*cursor2.position;
				cursor2.position = p;
				
				// Get cursor world position
				cursor2.positionWorld =
					new Vector3(data.positionWorld.x, data.positionWorld.y, data.positionWorld.z);
				
				// Grab
				if (data.openness < grabOpennessThreshold) {
					cursor2.grab();
				} else {
					cursor2.release();
				}
				
				// Update upper hand position
				if (pp.QueryGeoNode(PXCMGesture.GeoNode.Label.LABEL_BODY_HAND_SECONDARY |
					PXCMGesture.GeoNode.Label.LABEL_HAND_UPPER, out data)) {
					// Get cursor position in terms of GUI coordinate system (bottom-left origin)
					Vector2 uh = new Vector2(Screen.width - data.positionImage.x * Screen.width / frameSize.x,
						Screen.height - data.positionImage.y * Screen.height / frameSize.y);
					Vector2 up = (1.0f-smoothing)*uh + smoothing*cursor2.upperPosition;
					cursor2.upperPosition = up;
					cursor2.updateRotation();
				}
								
				cursor2.show();
			} else {
				cursor2.hide();
			}
			
			pp.ReleaseFrame();
		}
	}
}
