﻿using UnityEngine;
using System.Collections;

public class DebugCursor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Perceptual.Instance.onHover += this.onHover;
		Perceptual.Instance.onLeave += this.onLeave;
		Perceptual.Instance.onGrab += this.onGrab;
		Perceptual.Instance.onRelease += this.onRelease;
	}
	
	void onHover(int idx, GameObject obj) {
		Debug.Log("Hover " + idx + ": " + obj.name);
	}
	
	void onLeave(int idx, GameObject obj) {
		Debug.Log("Leave " + idx + ": " + obj.name);
	}

	void onGrab(int idx, GameObject obj) {
		Debug.Log("Grab " + idx + ": " + obj.name);
	}

	void onRelease(int idx, GameObject obj) {
		Debug.Log("Release " + idx + ": " + obj.name);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
