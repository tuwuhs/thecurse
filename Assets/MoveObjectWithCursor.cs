﻿using UnityEngine;
using System.Collections;

public class MoveObjectWithCursor : MonoBehaviour {
	public float zScale = 1.0f;
	
	Vector2 positionOffset;
	Vector3 positionWorldGrab;
	Vector3 objPositionWorldGrab;
	float zRotationGrab;
	float objZRotationGrab;
	int cursorIdx;
	bool grabbed;
	
	// Use this for initialization
	void Start () {
		Perceptual.Instance.onGrab += this.onGrab;
		Perceptual.Instance.onRelease += this.onRelease;
	}
	
	// Update is called once per frame
	void Update () {
		if (grabbed) {			
			Vector2 position = Perceptual.Instance.getCursorPosition(cursorIdx);
			Vector3 positionWorld = Perceptual.Instance.getCursorPositionWorld(cursorIdx);
			float zRotation = Perceptual.Instance.getCursorZRotation(cursorIdx);
			
			// Move the object...
			Vector3 screenPos = new Vector3();
			screenPos.x = position.x + positionOffset.x;
			screenPos.y = position.y + positionOffset.y;
			screenPos.z = transform.position.z - Camera.main.transform.position.z;
			Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPos);
			
			// Get Z (depth) by adding relative hand position
			// Note the difference in the coordinate systems:
			// - Intel SDK: X right, Y out of screen, Z up
			// - Unity camera: X right, Y up, Z into the screen
			worldPos.z = objPositionWorldGrab.z - zScale*(positionWorld.y - positionWorldGrab.y);
			transform.position = worldPos;
			
			// Rotate...
			Quaternion rot = transform.rotation;
			Vector3 rotAngle = rot.eulerAngles;
			rotAngle.z = objZRotationGrab + zRotation - zRotationGrab;
			rot.eulerAngles = rotAngle;
			transform.rotation = rot;
		}
	}
	
	void onGrab(int idx, GameObject obj) {
		if (obj != gameObject) return;
		if (!grabbed) {
			// Save cursor index
			cursorIdx = idx;
			
			// Get transformations
			Vector2 position = Perceptual.Instance.getCursorPosition(cursorIdx);
			Vector3 positionWorld = Perceptual.Instance.getCursorPositionWorld(cursorIdx);
			float zRotation = Perceptual.Instance.getCursorZRotation(cursorIdx);
			
			// Calculate the snapping offset
			Vector2 screenPos = Camera.main.WorldToScreenPoint(transform.position);
			positionOffset = screenPos - position;
			
			// Save the world position when start grabbing
			//   and hand position when start grabbing
			objPositionWorldGrab = transform.position;
			positionWorldGrab = positionWorld;
			
			// Save initial rotation
			objZRotationGrab = transform.rotation.eulerAngles.z;
			zRotationGrab = zRotation;
				
			grabbed = true;
		}
	}
	
	void onRelease(int idx, GameObject obj) {
		if (obj != gameObject) return;
		if (grabbed) {
			if (cursorIdx == idx) {
				grabbed = false;
			}
		}
	}
}
