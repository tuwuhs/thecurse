﻿using UnityEngine;
using System.Collections;

public class DrawPreview : MonoBehaviour {
	Texture2D texGesture;
	Texture2D texDepth;
	public int previewWidth = 240;

	private PXCUPipeline.Mode mode = PXCUPipeline.Mode.GESTURE | PXCUPipeline.Mode.DEPTH_QVGA;
	private PXCUPipeline pp;	
	
	void Start() {
		pp = new PXCUPipeline();
		pp.Init(mode);
		int[] size = new int[2];
		pp.QueryLabelMapSize(size);
		texGesture = new Texture2D(size[0], size[1], TextureFormat.RGB24, false);
		pp.QueryDepthMapSize(size);
		texDepth = new Texture2D(size[0], size[1], TextureFormat.RGB24, false);
	}
	
	void OnDisable() {
		pp.Dispose();
	}
	
	void Update() {
		if (!pp.AcquireFrame(false)) return;
		
		byte[] labelmap = new byte[texGesture.width*texGesture.height];
		int[] labels = new int[3];
		if (pp.QueryLabelMap(labelmap, labels)) {
		    Color32[] pixels = texGesture.GetPixels32(0);
			for (int i=0; i<texGesture.width*texGesture.height; i++) {
				pixels[i] = new Color32(
					(byte) (255-labelmap[i]), (byte) (255-labelmap[i]), (byte) (255-labelmap[i]), 255);
			}
	        texGesture.SetPixels32(pixels, 0);
			texGesture.Apply();
		}
		
		short[] depthPix = new short[texDepth.width*texDepth.height];
		if (pp.QueryDepthMap(depthPix)) {
		    Color32[] pixels = texDepth.GetPixels32(0);
			for (int i=0; i<texDepth.width*texDepth.height; i++) {
				pixels[i] = new Color32(
					(byte) depthPix[i], (byte) depthPix[i], (byte) depthPix[i], 255);
			}
	        texDepth.SetPixels32(pixels, 0);
			texDepth.Apply();
		}
		
		pp.ReleaseFrame();
	}
	
	void OnGUI() {
		int w = previewWidth;
		int h = texGesture.height * previewWidth/texGesture.width;
		
		Color colorOld = GUI.color;
		
		GUI.color = new Color(0.0f, 1.0f, 1.0f, 0.5f);
		GUI.DrawTextureWithTexCoords(new Rect(0, Screen.height - h, w, h),
			texDepth, new Rect(0, 0, -1, -1));
		
		GUI.color = new Color(1.0f, 1.0f, 0.0f, 0.5f);
		GUI.DrawTextureWithTexCoords(new Rect(0, Screen.height - h, w, h),
			texGesture, new Rect(0, 0, -1, -1));
	
		GUI.color = colorOld;
	}
}
